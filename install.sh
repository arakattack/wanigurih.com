#!/bin/bash
set -e
if [[ -f ".setupdone" ]]; then
    echo "Setup is done!"
    exit 1
fi
read -p "Domain: " domains
echo "setup domain " $domains
if [[ -z "$domains" ]]; then
    printf '%s\n' "No input entered"
    exit 1
else
    find . -type f \( -iname \*.yml -o -iname \*.conf \) -print0 | xargs -0 sed -i '' -e 's/example.com/'"$domains"'/g'
    mkdir -p ssl/live/$domains
    curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > ssl/options-ssl-nginx.conf
    curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > ssl/ssl-dhparams.pem
    data_path="./data/certbot"
    mkdir -p "$data_path/conf/live/$domains"
    cert_path="$data_path/conf/live/$domains"
    openssl rand -out /root/.rnd -hex 256
    openssl req -x509 -nodes -newkey rsa:1024 -days 1\
        -keyout $cert_path/privkey.pem \
        -out $cert_path/fullchain.pem \
        -subj /CN=localhost
    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $cert_path/fullchain.pem
    git clone https://github.com/laravel/laravel.git
    if [ $? -eq 0 ]; then
        mv README.md README.md.bak
        mv laravel/* .
        rm -rf laravel
        mv README.md.bak README.md
        touch .setupdone
    else
        exit 1
    fi
fi
