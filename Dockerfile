<<<<<<< HEAD
FROM arakattack/laravel-deployer:7.4
=======
# Stage 1 build php-fpm
FROM arakattack/laravel-deployer:latest as build
>>>>>>> f95c0d68c95556f9cb685803c5732ddf1d6b6930

ADD . /var/www/html
COPY . /var/www/html
WORKDIR /var/www/html

<<<<<<< HEAD
RUN touch database/database.sqlite
RUN touch storage/logs/laravel.log
RUN composer global require hirak/prestissimo
# RUN composer require predis/predis
=======
RUN touch storage/logs/laravel.log
RUN composer require predis/predis
>>>>>>> f95c0d68c95556f9cb685803c5732ddf1d6b6930
RUN composer install
RUN php artisan cache:clear
RUN php artisan view:clear
# RUN php artisan route:cache
COPY .env.example .env
RUN php artisan key:generate
RUN php artisan config:cache
# RUN npm install
# RUN npm run prod

RUN php artisan vendor:publish --all
RUN php artisan storage:link
RUN composer  dump-autoload

RUN chmod -R 777 /var/www/html/storage

#### Stage 2: Serve the php-fpm application from Nginx 
FROM nginx:latest

# Copy the react build from Stage 1
COPY --from=build /var/www/html/ /var/www/html/

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    rm -rf /etc/nginx/conf.d/*

COPY deploy/nginx/default.conf /etc/nginx/conf.d/

EXPOSE 5000

ENTRYPOINT ["nginx","-g","daemon off;"]

